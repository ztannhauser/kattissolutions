#! /usr/bin/env python

(h, m) = map(int, raw_input().split());

M = 24 * 60;

s = (h * 60 + m + M - 45) % M;

h2 = s / 60;
m2 = s % 60;

print("%i %i" % (h2, m2));
