#! /usr/bin/env python

total_vol = 0.0;

def calc_volume_of_tetrahedron(a, b, c, d):
	A = a[0] - b[0];
	B = a[1] - b[1];
	C = a[2] - b[2];
	D = a[0] - c[0];
	E = a[1] - c[1];
	F = a[2] - c[2];
	G = a[0] - d[0];
	H = a[1] - d[1];
	I = a[2] - d[2];
	return abs(A*(E*I-F*H)-B*(D*I-F*G)+C*(D*H-E*G)) / 6.0;

N = int(raw_input());
for I in range(0, N):
	num_faces = int(raw_input());
	grand_avg_point = [0, 0, 0];
	faces = [];
	for f in range(0, num_faces):
		vs = map(float, raw_input().split());
		num_vertices = int(vs[0]);
		points = [];
		avg_point = [0, 0, 0];
		for i in range(0, num_vertices):
			X = vs[1 + i * 3];
			Y = vs[2 + i * 3];
			Z = vs[3 + i * 3];
			avg_point[0] += X;
			avg_point[1] += Y;
			avg_point[2] += Z;
			points.append([X, Y, Z]);
		avg_point[0] /= num_vertices;
		avg_point[1] /= num_vertices;
		avg_point[2] /= num_vertices;
		grand_avg_point[0] += avg_point[0];
		grand_avg_point[1] += avg_point[1];
		grand_avg_point[2] += avg_point[2];
		faces.append((avg_point, points));
	grand_avg_point[0] /= num_faces;
	grand_avg_point[1] /= num_faces;
	grand_avg_point[2] /= num_faces;
	for f in faces:
		l = len(f[1]);
		for i in range(0, l):
			ip1 = (i + 1) % l;
			total_vol += calc_volume_of_tetrahedron(grand_avg_point,
				f[0], f[1][i], f[1][ip1]);

print("%.2f" % (total_vol));
