#! /usr/bin/env python

import math;

g = 9.81

n = int(raw_input());
for i in range(0, n):
	(v_0, delta, x_1, h_1, h_2) = map(float, raw_input().split());
	delta = math.radians(delta);
	t = x_1 / (math.cos(delta) * v_0);
	y = v_0 * t * math.sin(delta) - (1.0/2.0) * g * t ** 2;
	if(h_1 + 1 <= y and y <= h_2 - 1):
		print("Safe");
	else:
		print("Not Safe");
	
