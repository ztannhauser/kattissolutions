#! /usr/bin/env python

N = int(raw_input());
for i in range(0, N):
	(b, p) = map(float, raw_input().split());
	bpm = 60 * b / p;
	abpm = 60 / (p / b);
	print("%f %f" % (bpm, abpm));
