#! /usr/bin/env python

d = float(raw_input());
n = int(raw_input());
area = 0.0;

for i in range(0, n):
	(w, h) = map(float, raw_input().split());
	area = area + (w * h);

print(area * d);
