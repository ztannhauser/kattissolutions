#! /usr/bin/env python

l = raw_input();

V = [1, 0, 0];

def swap(index1, index2):
	t = V[index1];
	V[index1] = V[index2];
	V[index2] = t;

for c in l:
	if(c == 'A'):
		swap(0, 1);
	elif(c == 'B'):
		swap(1, 2);
	elif(c == 'C'):
		swap(0, 2);

print(V.index(1) + 1);
