#! /usr/bin/env python

import math;

r = float(raw_input());

area_of_circ1 = math.pi * pow(r, 2);
area_of_circ2 = 2 * pow(r, 2);

print("%.6f" % area_of_circ1);
print("%.6f" % area_of_circ2);
