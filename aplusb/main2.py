#! /usr/bin/env python

N = int(raw_input());
nums = map(int, raw_input().split());
nums.sort();
lowest = nums[0];
for i in range(0, len(nums)):
	nums[i] = nums[i] - lowest + 1;


last_value = nums[0];
occurrences = [1];
diff_nums = [last_value];

for i in range(1, len(nums)):
	if(last_value == nums[i]):
		occurrences[len(occurrences) - 1] += 1;
	else:
		diff_nums.append(nums[i]);
		occurrences.append(1);
		last_value = nums[i];
		
print(occurrences);
print(diff_nums);
highest = diff_nums[len(diff_nums) - 1];
total = 0;

sums_for_number = [0];

for i in range(0, len(diff_nums)):
	num = diff_nums[i];
	for j in range(0, i):
		sume = diff_nums[i] + diff_nums[j];
		if(sume > highest):
			break;
		while(len(sums_for_number) <= sume):
			sums_for_number.append(0);
		sume -= 1;
		sums_for_number[sume] += occurrences[i] * occurrences[j];
	total += sums_for_number[num - 1] * occurrences[i];

print(total);
print(sums_for_number);
print(range(1, 1 + len(sums_for_number)));







