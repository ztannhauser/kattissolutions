#! /usr/bin/env python

N = int(raw_input());
nums = map(int, raw_input().split());
nums.sort();

offset = nums[0] - 1;
for i in range(0, len(nums)):
	nums[i] = nums[i] - offset;

last_value = nums[0];
occurrences = [1];
diff_nums = [last_value];

for i in range(1, len(nums)):
	if(last_value == nums[i]):
		occurrences[len(occurrences) - 1] += 1;
	else:
		diff_nums.append(nums[i]);
		occurrences.append(1);
		last_value = nums[i];

print(occurrences);
print(diff_nums);

total = 0;
for i in range(0, len(diff_nums)):
	num = diff_nums[i];
	start = 0;
	end = i - 1;
	while(start < end):
		t = diff_nums[start] + diff_nums[end] + offset;
		print("%i + %i == %i" % (diff_nums[start], diff_nums[end], t));
		if(t == num):
			multipler = 2 * occurrences[start] * occurrences[end] * occurrences[i];
			print("%i + %i == %i, multipler == %i" %
				(diff_nums[start], diff_nums[end], num, multipler));
			total += multipler;
			start += 1;
			end -= 1;
		elif(t > num):
			end -= 1;
		else: # t < num
			start += 1;
	if(start == end):
		occur = occurrences[start];
		if(occur > 1):
			some = diff_nums[start] + diff_nums[end] + offset;
			if(diff_nums.count(some)):
				total += occur * occur - occur;
print(total);





