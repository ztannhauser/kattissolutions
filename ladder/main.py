#! /usr/bin/env python

import math;

(rise, a) = map(int, raw_input().split());

run = rise / math.tan(math.radians(a));

print(int(math.ceil(math.hypot(rise, run))));
