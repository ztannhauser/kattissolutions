#! /usr/bin/env python

(a, b) = raw_input().split();

def reverse(s):
	ret = "";
	for i in range(0, len(s)):
		ret = s[i] + ret;
	return ret;

A = int(reverse(a));
B = int(reverse(b));

print(max(A, B));
