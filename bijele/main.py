#! /usr/bin/env python

P = map(int, raw_input().split());

M = [1, 1, 2, 2, 2, 8];

for i in range(0, len(P)):
	P[i] = M[i] - P[i];

print("%i %i %i %i %i %i" % (P[0], P[1], P[2], P[3], P[4], P[5]));
