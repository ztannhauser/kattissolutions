#! /usr/bin/env python

def get_sum_of_digits(num):
	s = "%i" % num;
	ret = 0;
	for e in s:
		ret = ret + (ord(e) - ord('0'));
	return ret;

while True:
	N = int(raw_input());
	if(N == 0):
		break;
	else:
		sumN = get_sum_of_digits(N);
		P = 11;
		while True:
			if(sumN == get_sum_of_digits(P * N)):
				break;
			P = P + 1;
		print(P);
