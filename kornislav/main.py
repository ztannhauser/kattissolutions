#! /usr/bin/env python

import copy;

nums = map(int, raw_input().split());

def c(x):
	if(len(x) == 1):
		return [x];
	else:
		ret = [];
		for i in range(0, len(x)):
			y = copy.deepcopy(x);
			y.pop(i);
			a = c(y);
			for q in a:
				q.insert(0, x[i]);
				ret.append(q);
		return ret;

combinations = c(nums);

# x1 >= x2;
# y1 <= y2;

area = 0;

for c in combinations:
	(x1, y1, x2, y2) = c;
	if(x1 >= x2 and y1 <= y2):
		my_area = x2 * y1;
		area = max(my_area, area);

print(area);










