#! /usr/bin/env python

def is_possible(nums):
	if(nums[0] + nums[1] == nums[2]):
		return True;
	if(nums[0] - nums[1] == nums[2]):
		return True;
	if(nums[1] - nums[0] == nums[2]):
		return True;
	if(nums[0] * nums[1] == nums[2]):
		return True;
	if(nums[0] / nums[1] == nums[2]):
		return True;
	if(nums[1] / nums[0] == nums[2]):
		return True;
	return False;

n = int(raw_input());
for i in range(0, n):
	nums = map(float, raw_input().split());
	if(is_possible(nums)):
		print("Possible");
	else:
		print("Impossible");
