#! /usr/bin/env python

import math;

x = int(raw_input());
a = pow(4, x);
b = int(math.sqrt(a));
s = a + b * 2 + 1;

print(s);
