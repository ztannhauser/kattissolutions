#! /usr/bin/env python

N = int(raw_input());
for I in range(0, N):
	n = int(raw_input());
	nums = [];
	x = map(int, raw_input().split());
	for c in x:
		if(nums.count(c)):
			nums.remove(c);
		else:
			nums.append(c);
	print("Case #%i: %i" % (I + 1, nums[0]));
