#! /usr/bin/env python

highest_score = 0;
highest_score_index = 0;

for i in range(0, 5):
	nums = map(int, raw_input().split());
	s = sum(nums);
	if(highest_score < s):
		highest_score = s;
		highest_score_index = i;

print("%i %i" % (highest_score_index + 1, highest_score));
