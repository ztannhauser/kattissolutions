#! /usr/bin/env python

import math;

v = int(raw_input());

print("%i:" % v);

o = [];

for y in range(2, v):
	q = math.floor(y / 2);
	w = y - q;
	x1 = (v - w) / (y);
	if(int(x1) == x1):
		o.append((x1 + 1, x1));
	x2 = float(v) / y;
	if(int(x2) == x2):
		o.append((x2, x2));

o.sort();

for w in o:
	print("%i,%i" % (int(w[0]), int(w[1])));

