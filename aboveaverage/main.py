#! /usr/bin/env python

N = int(raw_input());
for I in range(0, N):
	nums = map(float, raw_input().split());
	n = int(nums[0]);
	total = 0.0;
	for i in range(1, n + 1):
		total += nums[i];
	average = total / n;
	total_students_above_average = 0.0;
	for i in range(1, n + 1):
		if(nums[i] > average):
			total_students_above_average = total_students_above_average + 1.0;
	average_students_above_average = total_students_above_average / n;
	print("%.3f%%" % (average_students_above_average * 100));
