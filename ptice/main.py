#! /usr/bin/env python

n = int(raw_input());

line = raw_input();

adrain_score = 0;
bruno_score = 0;
goran_score = 0;

adrain_guesses = "ABC";
bruno_guesses = "BABC";
goran_guesses = "CCAABB";

for i in range(0, len(line)):
	e = line[i];
	if(e == adrain_guesses[i % len(adrain_guesses)]):
		adrain_score = adrain_score + 1;
	if(e == bruno_guesses[i % len(bruno_guesses)]):
		bruno_score = bruno_score + 1;
	if(e == goran_guesses[i % len(goran_guesses)]):
		goran_score = goran_score + 1;

highest_score = max(adrain_score, bruno_score, goran_score);
print(highest_score);

if(adrain_score == highest_score):
	print("Adrian");
if(bruno_score == highest_score):
	print("Bruno");
if(goran_score == highest_score):
	print("Goran");
