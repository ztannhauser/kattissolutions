#! /usr/bin/env python

nums = map(int, raw_input().split());
nums.sort();
line = raw_input();
strlen = len(line);
indexes = [];

for i in range(0, strlen):
	indexes.append(ord(line[i]) - ord('A'));

print("%i %i %i" % (nums[indexes[0]], nums[indexes[1]], nums[indexes[2]]));
