#! /usr/bin/env python

words = raw_input().split();
unique_words = [];
for word in words:
	if(unique_words.count(word)):
		print("no");
		exit(0);
	else:
		unique_words.append(word);
print("yes");
