#! /usr/bin/env python

v = [
		(4, 4 / 4 / 4 / 4),
		(3, 4 / 4 / 4),
		(2, 4 / 4),
		(1, 4),
		(2, 4 * 4),
		(3, 4 * 4 * 4),
		(4, 4 * 4 * 4 * 4),
	];

def find_nearest(x, l):
	b = 4 - l;
	e = b + 2 * l;
	print(b);
	print(e);
	if(b == e - 1):
		return b;
	for i in range(b, e):
		ii = i + 1;
		if(v[i] <= x and x <= v[ii]):
			dbx = x - v[i];
			dex = v[ii] - x;
			return [dex, dbx][dbx > dex];
	return e;

def do(x):
	s = 0;
	l = 4;
	used = [];
	while(l):
		no_negate = (l == 4) or (s < x);
		diff = abs(x - s);
		index = find_nearest(diff, l);
		if(index < 0):
			return "no solution";
		else:
			e = v[index];
			used.append((no_negate, e));
			l = l - e[0];
			s = s + [-1, 1][no_negate] * e[1];
	print(used);

n = int(raw_input());
for i in range(0, n):
	x = int(raw_input());
	print(x);
	print do(x);
