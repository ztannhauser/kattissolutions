#! /usr/bin/env python

N = int(raw_input());
P = map(float, raw_input().split());

total_area = 0.0;

multipler = pow(2, 29.0);
goal_area = pow(2, 30.0);

width = pow(2.0, -5/4.0);
height = pow(2.0, -3/4.0);

total_per = 0.0;

for i in range(0, len(P)):
	e = P[i];
	newtotal = total_area + multipler * e;
	if(newtotal > goal_area):
		e = (goal_area - total_area) / multipler;
		total_area = goal_area;
	else:
		total_area = newtotal;
	total_per = total_per + e * (2 * (width + height));
	if(total_area == goal_area):
		break;
	multipler = multipler / 2;
	if(i % 2):
		width = width / 2;
	else:
		height = height / 2;

if(total_area == goal_area):
	total_per = total_per - (4 * pow(2, -5/4.0) + 2 * pow(2, -3/4.0));
	print(total_per / 2);
else:
	print("impossible");
