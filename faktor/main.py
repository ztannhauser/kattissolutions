#! /usr/bin/env python

(a, b) = map(int, raw_input().split());
print(a * b - a + 1);
