#! /usr/bin/env python

import math;

(n, w, h) = map(int, raw_input().split());
d = math.hypot(w, h);
for i in range(0, n):
	if(float(raw_input()) <= d):
		print("DA");
	else:
		print("NE");
