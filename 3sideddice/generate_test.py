#! /usr/bin/env python

import random;

for i in range(0, 50):
	a = random.randrange(1, 10000);
	b = random.randrange(1, 10000);
	c = random.randrange(1, 10000);
	k = random.randrange(1, 10000);
	j = random.randrange(1, 10000);
	h = random.randrange(1, 10000);
	I = i % 3;
	if(I == 0):
		print("%i %i %i" % (a, b, c));
		print("%i %i %i" % (a * k, b * k, c * k));
		print("%i %i %i" % (a * j, b * j, c * j));
	elif(I == 1):
		z = random.randrange(1, 10000);
		print("%i %i %i" % (a + z, b, c));
		print("%i %i %i" % (a * k, b * k + z, c * k));
		print("%i %i %i" % (a * j, b * j, c * j + z));
	else:
		z = random.randrange(1, 10000);
		print("%i %i %i" % (a + z, b + z, c + z));
		print("%i %i %i" % (a * k, b * k, c * k));
		print("%i %i %i" % (a * j, b * j, c * j));
	print("%i %i %i" % (a * h, b * h, c * h));
	print("");
print("0 0 0");
