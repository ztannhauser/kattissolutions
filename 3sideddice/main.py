#! /usr/bin/env python

def almost_zero(x):
	return abs(x) < 0.0001;

def scalar_multiply(row, k):
	for i in range(0, 4):
		row[i] = (row[i] * k);
		if(almost_zero(row[i])):
			row[i] = 0;

def row_subtract_and_scale(row_target, row_source, k):
	for i in range(0, len(row_target)):
		row_target[i] = row_target[i] + k * row_source[i];
		if(almost_zero(row_target[i])):
			row_target[i] = 0;

def rref(M):
	i = 0;
	m = min(len(M), len(M[0]));
	for c in range(0, m):
		for r in range(i, m):
			if(M[r][c]):
				t = M[r];
				M[r] = M[i];
				M[i] = t;
				for j in range(0, m):
					if(j == i):
						scalar_multiply(M[i], 1.0 / M[i][c]); 
					else:
						row_subtract_and_scale(M[j], M[i], -M[j][c] / M[i][c]);
				i = i + 1;

def judge(m):
	for r in range(0, 2):
		all_zeros = True;
		for c in range(0, 2):
			if(m[r][c]):
				all_zeros = False;
				break;
		if(all_zeros):
			if(m[r][3] != 0):
				return False;
		else:
			if(m[r][3] == 0):
				return False;
	for r in range(0, 2):
		if(m[r][3] < 0):
			return False;
	return True;

while(True):
	(a, b, c) = map(float, raw_input().split());
	if(a == 0 and b == 0 and c == 0):
		exit(0);
	else:
		(d, e, f) = map(float, raw_input().split());
		(g, h, i) = map(float, raw_input().split());
		(j, k, l) = map(float, raw_input().split());
		m = [[a, d, g, j], [b, e, h, k], [c, f, i, l]];
		for r in m:
			print(r);
		rref(m);
		for r in m:
			print(r);
		if(judge(m)):
			print("YES");
		else:
			print("NO");
		raw_input();





