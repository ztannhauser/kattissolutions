#! /usr/bin/env python

n = int(raw_input());
for i in range(0, n):
	num = int(raw_input());
	if(num % 2):
		print("%i is odd" % num);
	else:
		print("%i is even" % num);
