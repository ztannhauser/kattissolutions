/*/
	deps = 
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/new.c",
		"~/deps/array/push_n.c",
		"~/deps/array/delete.c",
		"~/deps/misc/assert.h",
		"~/deps/misc/assert.c",
	}
/*/

#include <unistd.h>

#define LEN 500000
#define LOOP 1000000007

int main()
{
	char input[LEN];
	int len = read(STD_IN, input, LEN) - 1;
	input[len] = 0;
	struct array mark_indexes = array_new(int);
	size_t i;
	for(i = 0;i < len;i++)
	{
		if(input[i] == '?')
		{
			array_push_n(&mark_indexes, &i);
		}
	}
	
	size_t count_shifts_needed()
	{
		size_t sum = 0;
		size_t num_of_zeros = 0;
		size_t i;
		for(i = 0;i < len;i++)
		{
			if(input[i] == '0')
			{
				sum = (sum + ( i - num_of_zeros )) % LOOP;
				num_of_zeros++;
			}
		}
		return sum;
	}
	
	int sum = 0;
	size_t n = 1 << mark_indexes.n;
	for(i = 0;i < n;i++)
	{
		int j;
		for(j = 0;j < mark_indexes.n;j++)
		{
			int index = array_index(mark_indexes, j, int);
			input[index] = (0b1 & (i >> j)) ? '1' : '0';
		}
		printf("%s\n", input);
		sum = (sum + count_shifts_needed()) % LOOP;
	}
	array_delete(&mark_indexes);
	printf("%i\n", sum);
	return 0;
}
