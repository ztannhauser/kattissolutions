#! /usr/bin/env python

LOOP = 1000000007

line = raw_input();
strlen = len(line);

# tuples are {[0] = index, [1] = n, [2] = sum}
q = [[-1, 0, 0]];

for i in range(0, strlen):
	c = line[i];
	if(c == '?'):
		q.append([i, 0, 0]);
	elif(c == '0'):
		nm1 = len(q) - 1;
		w = q[nm1];
		w[2] += i - (w[0] + w[1] + 1);
		w[1] = w[1] + 1;
		q[nm1] = w;

s = 0;
n = 0;
p = 1;
nm1 = len(q) - 1;
for i in range(nm1, -1, -1):
	e = q[i];
	if(i < nm1):
		l = q[i + 1];
		diff = l[0] - (e[0] + e[1] + 1);
		s = (s + diff * n) % LOOP;
	s = (s + e[2] * p) % LOOP;
	n = (n + e[1] * p) % LOOP;
	if(i > 0):
		s = (2 * s + n) % LOOP;
		n = (2 * n + p) % LOOP;
		p = (2 * p) % LOOP;
print(s);
