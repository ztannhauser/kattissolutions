#! /usr/bin/env python

LOOP = 1000000007

line = raw_input();
strlen = len(line);

s = 0;
n = 0;
p = 1;
for i in range(strlen - 1, -1, -1):
	c = line[i];
	if(c == '0'):
		n = (n + p) % LOOP;
	elif(c == '1'):
		s = (s + n) % LOOP;
	else:
		s = (2 * s + n) % LOOP;
		n = (2 * n + p) % LOOP;
		p = (2 * p) % LOOP;
print(s);








