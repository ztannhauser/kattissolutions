/*/
	deps = 
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/new.c",
		"~/deps/array/push_n.c",
		"~/deps/misc/assert.c",
		"~/deps/misc/assert.h",
	}
/*/

#include <stdio.h>
#include <unistd.h>

#define LEN 500000
#define LOOP 1000000007

struct Q {int index; int n; int sum; };

int main()
{
	set_verbose(false);
	char input[LEN];
	int len = read(STD_IN, input, LEN) - 1;
	struct array q = array_new(struct Q);
	{
		struct Q w = 
		{
			.index = -1,
			.n = 0,
			.sum = 0,
		};
		array_push_n(&q, &w);
	}
	int i;
	for(i = 0;i < len;i++)
	{
		switch(input[i])
		{
			case '?':
			{
				struct Q w = 
				{
					.index = i,
					.n = 0,
					.sum = 0,
				};
				array_push_n(&q, &w);
				break;
			}
			case '0':
			{
				struct Q* w = arrayp_index(q, q.n - 1);
				verpv(i);
				verpv(w->index);
				w->sum += i - (w->index + w->n + 1);
				w->n++;
				verpv(w->sum);
				verpv(w->n);
				break;
			}
		}
	}
	HERE;
	size_t sum = 0;
	size_t n = 0; // number of zeros in permutations processed
	size_t p = 1; // number of permutations
	size_t s = q.n - 1;
	for(i = s;i >= 0;i--)
	{
		HERE;
		struct Q e = array_index(q, i, struct Q);
		verpv(i);
		verpv(e.index);
		verpv(e.n);
		verpv(e.sum);
		
		if(i < s)
		{
			struct Q l = array_index(q, i + 1, struct Q);
			int diff = l.index - (e.index + e.n + 1);
			verpv(diff);
			sum = (sum + diff * n) % LOOP;
		}
		
		sum = (sum + e.sum * p) % LOOP;
		n = (n + e.n * p) % LOOP;
		
		verpv(sum);
		verpv(n);
		if(i > 0)
		{
			sum = (sum * 2 + n) % LOOP;
			n = (2 * n + p) % LOOP;
			p = (2 * p) % LOOP;
		}
		verpv(sum);
		verpv(n);
		verpv(p);
	}
	HERE;
	verpv(sum);
	verpv(n);
	verpv(p);
	printf("%lu\n", sum);
	return 0;
}













