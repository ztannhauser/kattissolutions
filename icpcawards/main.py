#! /usr/bin/env python

n = int(raw_input());

best_universities = [];
best_teams = [];

for i in range(0, n):
	(university_name, team_name) = raw_input().split();
	if(best_universities.count(university_name) == 0):
		best_universities.append(university_name);
		best_teams.append(team_name);
	if(len(best_universities) > 12):
		break;

for i in range(0, 12):
	print("%s %s" % (best_universities[i], best_teams[i]));
