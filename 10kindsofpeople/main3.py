#!/usr/bin/env python

import heapq
import math

(rows, cols) = map(int, raw_input().split());

grid = [];

ids = [];
for i in range(0, rows):
	ids.append([0] * cols);
	
todo = [(0, 0)];

directions = [[-1, 0], [1, 0], [0, 1], [0, -1]];

def paint_bucket():
	i = 1;
	area = rows * cols;
	while(len(todo) and area > 0):
		O = todo.pop();
		if(ids[O[0]][O[1]] == 0):
			todo2 = [O];
			t = grid[O[0]][O[1]];
			while(len(todo2)):
				loc = todo2.pop();
				ids[loc[0]][loc[1]] = i;
				area = area - 1;
				for direct in directions:
					X = loc[0] + direct[0];
					Y = loc[1] + direct[1];
					if(0 <= X and X < rows and 0 <= Y and Y < cols):
						if(ids[X][Y] == 0):
							if(grid[X][Y] == t):
								todo2.append((X, Y));
							else:
								todo.append((X, Y));
		i = i + 1;

for i in range(0, rows):
	line = raw_input();
	new_row = [];
	for j in range(0, cols):
		new_row.append(line[j] == '1');
	grid.append(new_row);

paint_bucket();

n = int(raw_input());
for i in range(0, n):
	(r_1, c_1, r_2, c_2) = map(int, raw_input().split());
	r_1 -= 1;
	c_1 -= 1;
	r_2 -= 1;
	c_2 -= 1;
	if(ids[r_1][c_1] == ids[r_2][c_2]):
		if(grid[r_1][c_1]):
			print "decimal";
		else:
			print "binary";
	else:
		print "neither";






