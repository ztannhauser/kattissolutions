#!/usr/bin/env python

import heapq
import math

class MyHeap(object):
	def __init__(self, key=lambda x:x):
		self.key = key
		self._data = []

	def push(self, item):
		heapq.heappush(self._data, (self.key(item), item))

	def pop(self):
		return heapq.heappop(self._data)[1]

(rows, cols) = map(int, raw_input().split());

grid = [];
untouched_space = [];

for i in range(0, rows):
	untouched_space.append([0] * cols);

untouched_count = 1;

directions = [[-1, 0], [1, 0], [0, 1], [0, -1]];

def is_reachable(t, src, dest):
	h = MyHeap(key = lambda x: math.hypot(x[0] - dest[0], x[1] - dest[1]));
	h.push(src);
	untouched_space[src[0]][src[1]] = untouched_count;
	while(len(h._data)):
		e = h.pop();
		for direct in directions:
			X = e[0] + direct[0];
			Y = e[1] + direct[1];
			if(X == dest[0] and Y == dest[1]):
				return True;
			if(0 <= X and X < rows and 0 <= Y and Y < cols):
				if(grid[X][Y] == t):
					us = untouched_space[X][Y];
					if(us < untouched_count):
						h.push((X, Y));
						untouched_space[X][Y] = untouched_count;
	return False;

for i in range(0, rows):
	line = raw_input();
	new_row = [];
	for j in range(0, cols):
		new_row.append(line[j] == '1');
	grid.append(new_row);

n = int(raw_input());
for i in range(0, n):
	(r_1, c_1, r_2, c_2) = map(int, raw_input().split());
	r_1 -= 1;
	c_1 -= 1;
	r_2 -= 1;
	c_2 -= 1;
	t = grid[r_1][c_1];
	def print_connected():
		if(t):
			print "decimal";
		else:
			print "binary";
	if(t == grid[r_2][c_2]):
		us1 = untouched_space[r_1][c_1];
		us2 = untouched_space[r_2][c_2];
		if(us1 > 0 and us1 == us2):
			print_connected();
		else:
			is_reach = is_reachable(t, (r_1, c_1), (r_2, c_2));
			untouched_count = untouched_count + 1;
			if(is_reach):
				print_connected();
			else:
				print "neither";
	else:
		print "neither";











