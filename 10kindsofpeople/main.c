/*/
	deps = 
	{
		"~/deps/typedefs.h",
		"~/deps/defines.h",
		"~/deps/macros.h",
		"~/deps/debug.c",
		"~/deps/array/struct.h",
		"~/deps/array/new.c",
		"~/deps/array/heap/struct.h",
		"~/deps/array/heap/push_n.c",
		"~/deps/array/heap/pop_0.c",
		"~/deps/array/heap/delete.c",
		"~/deps/misc/assert.h",
		"~/deps/misc/assert.c",
	}
	linker_flags = 
	{
		"-lm"
	}
/*/

#include <stdio.h>
#include <string.h>
#include <math.h>

struct point
{
	int x, y;
};

struct point dest;

int compare(struct point* a, struct point* b)
{
	double dist(struct point* a, struct point* b)
	{
		return hypot(b->x - a->x, b->y - a->y);
	}
	double a_dist = dist(a, &dest);
	double b_dist = dist(b, &dest);
	return a_dist - b_dist;
}

struct point directions[4] = {{-1, 0}, {0, 1}, {1, 0}, {0, -1}};

int main()
{
	set_verbose(true);
	int r, c;
	scanf("%i %i\n", &r, &c);
	bool map[r][c];
	bool newspace[r][c];
	{
		int i, j;
		for(i = 0;i < r;i++)
		{
			for(j = 0;j < c;j++)
			{
				map[i][j] = (getchar() == '1');
			}
			assert(getchar() == '\n', "!!");
		}
	}
	bool kind;
	bool is_reachable(struct point src)
	{
		struct point swap;
		struct heap h = 
		{
			.array = array_new(struct point),
			.compare = compare,
			.swapspace = &swap
		};
		heap_push_n(&h, &src);
		newspace[src.x][src.y] = false;
		bool ret = false;
		while(h.array.n)
		{
			struct point e = array_index(h.array, 0, struct point);
			//verpv(e.x);
			//verpv(e.y);
			if(e.x == dest.x && e.y == dest.y)
			{
				ret = true;
				break;
			}
			heap_pop_0(&h);
			int i;
			for(i = 0;i < 4;i++)
			{
				struct point direct = directions[i];
				int X = e.x + direct.x;
				int Y = e.y + direct.y;
				if(0 <= X && X < r && 0 <= Y && Y < c)
				{
					bool W = map[X][Y] == kind;
					bool E = newspace[X][Y];
					if(W && E)
					{
						heap_push_n(&h, varptr((struct point){X, Y}));
						newspace[X][Y] = false;
					}
				}
			}
		}
		heap_delete(&h);
		return ret;
	}
	int n;
	scanf("%i\n", &n);
	while(n--)
	{
		memset(newspace, true, r * c);
		struct point src;
		scanf("%i %i %i %i\n", &src.x, &src.y, &dest.x, &dest.y);
		src.x -= 1;
		src.y -= 1;
		dest.x -= 1;
		dest.y -= 1;
		if((kind = map[src.x][src.y]) != map[dest.x][dest.y])
		{
			printf("neither\n");
		}
		else
		{
			bool is = is_reachable(src);
			if(is)
			{
				printf("%s\n", kind ? "decimal" : "binary");
			}
			else
			{
				printf("neither\n");
			}
		}
	}
	return 0;
}















