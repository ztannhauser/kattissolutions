#!/usr/bin/env python

import random;

size = 1000;

print(str(size) + " " + str(size));

grid = [];
for i in range(0, size):
	newrow = [];
	for j in range(0, size):
		newrow.append(random.randrange(0, 2));
	grid.append(newrow);

def pretty_print():
	for r in range(0, size):
		line = "";
		for c in range(0, size):
			line = line + str(grid[r][c]);
		print(line);

pretty_print();
print("1000");
for i in range(0, 1000):
	c = [];
	for f in range(0, 4):
		c.append(random.randrange(1, size + 1));
	print(str(c[0]) + " " + str(c[1]) + " " + str(c[2]) + " " + str(c[3]));
