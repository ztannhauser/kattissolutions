#! /usr/bin/env python

N = int(raw_input());
for i in range(0, N):
	line = raw_input();
	if(line == "P=NP"):
		print("skipped");
	else:
		(a, b) = map(int, line.split("+"));
		print(a + b);
