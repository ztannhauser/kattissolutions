#! /usr/bin/env python

(l, r) = map(int, raw_input().split());

if(l == r):
	if(l == 0):
		print("Not a moose");
	else:
		print("Even %i" % (l + r));
else:
	print("Odd %i" % (max(l, r) * 2));
