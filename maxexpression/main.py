import bisect

n = int(raw_input());
equation = raw_input();

pos_products = [];
neg_products = [];

is_term_positive = True;
number_of_terms = 0;

def push(is_term_positive, number_of_terms):
	if(is_term_positive):
		bisect.insort(pos_products, (-number_of_terms, number_of_terms, []));
	else:
		bisect.insort(neg_products, (-number_of_terms, number_of_terms, []));

for i in range(0, len(equation)):
	c = equation[i];
	if(c == '+'):
		push(is_term_positive, number_of_terms);
		is_term_positive = True;
		number_of_terms = 0;
	elif(c == '-'):
		push(is_term_positive, number_of_terms);
		is_term_positive = False;
		number_of_terms = 0;
	elif(c == '?'):
		number_of_terms = number_of_terms + 1;

push(is_term_positive, number_of_terms);

values = [];

(num_of_zeros, num_of_ones, num_of_twos) = map(int, raw_input().split());

for i in range(0, num_of_zeros):
	values.append(0);
for i in range(0, num_of_ones):
	values.append(1);
for i in range(0, num_of_twos):
	values.append(2);

if(values[0] == 0):
	for ele in neg_products:
		ele[2].append(values.pop(0));

for ele in pos_products:
	while(ele[1] > len(ele[2])):
		ele[2].append(values.pop());

if(len(values) > 0):
	if(values[len(values) - 1] == 2):
		while(len(values) > 0):
			for ele in neg_products:
				if(ele[1] > len(ele[2])):
					ele[2].append(values.pop());
	else:
		for ele in neg_products:
			while(ele[1] > len(ele[2])):
				ele[2].append(values.pop());

total = 0;

for ele in pos_products:
	term = 1;
	for t in ele[2]:
		term = term * t;
	total = total + term;
		
for ele in neg_products:
	term = 1;
	for t in ele[2]:
		term = term * t;
	total = total - term;

print(total);







