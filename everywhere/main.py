#! /usr/bin/env python

N = int(raw_input());
for i in range(0, N):
	n = int(raw_input());
	cites = [];
	for j in range(0, n):
		new_city = raw_input();
		if(cites.count(new_city) == 0):
			cites.append(new_city);
	print(len(cites));
