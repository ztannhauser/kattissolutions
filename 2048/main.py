board = [];
for i in range(0, 4):
	board.append(map(int, raw_input().split()));
direction = int(raw_input());
if direction == 0:
	for r in range(0, 4):
		new_row = [];
		for c in range(0, 3, 1):
			if(board[r][c] != 0):
				o = c + 1;
				found_something = True;
				while(board[r][o] == 0):
					if(o == 3):
						found_something = False;
						break;
					o = o + 1;
				if(found_something):
					if(board[r][c] == board[r][o]):
						board[r][c] *= 2;
						board[r][o] = 0;
					new_row.append(board[r][c]);
				else:
					new_row.append(board[r][c]);
		if(board[r][3] != 0):
				new_row.append(board[r][3]);
		while(len(new_row) < 4):
			new_row.append(0);
		board[r] = new_row;
elif direction == 1:
	for c in range(0, 4):
		new_row = [];
		for r in range(0, 3):
			if(board[r][c] != 0):
				o = r + 1;
				found_something = True;
				while(board[o][c] == 0):
					if(o == 3):
						found_something = False;
						break;
					o = o + 1;
				if(found_something):
					if(board[r][c] == board[o][c]):
						board[r][c] *= 2;
						board[o][c] = 0;
					new_row.append(board[r][c]);
				else:
					new_row.append(board[r][c]);
		if(board[3][c] != 0):
				new_row.append(board[3][c]);
		while(len(new_row) < 4):
			new_row.append(0);
		for r in range(0, 4):
			board[r][c] = new_row[r];
elif direction == 2:
	for r in range(0, 4):
		new_row = [];
		for c in range(3, 0, -1):
			if(board[r][c] != 0):
				o = c - 1;
				found_something = True;
				while(board[r][o] == 0):
					if(o == 0):
						found_something = False;
						break;
					o = o - 1;
				if(found_something):
					if(board[r][c] == board[r][o]):
						board[r][c] *= 2;
						board[r][o] = 0;
					new_row.insert(0, board[r][c]);
				else:
					new_row.insert(0, board[r][c]);
		if(board[r][0] != 0):
				new_row.insert(0, board[r][0]);
		while(len(new_row) < 4):
			new_row.insert(0, 0);
		board[r] = new_row;
elif direction == 3:
	for c in range(0, 4):
		new_row = [];
		for r in range(3, 0, -1):
			if(board[r][c] != 0):
				o = r - 1;
				found_something = True;
				while(board[o][c] == 0):
					if(o == 0):
						found_something = False;
						break;
					o = o - 1;
				if(found_something):
					if(board[r][c] == board[o][c]):
						board[r][c] *= 2;
						board[o][c] = 0;
					new_row.insert(0, board[r][c]);
				else:
					new_row.insert(0, board[r][c]);
		if(board[0][c] != 0):
				new_row.insert(0, board[0][c]);
		while(len(new_row) < 4):
			new_row.insert(0, 0);
		for r in range(0, 4):
			board[r][c] = new_row[r];

for i in range(0, 4):
	print("%i %i %i %i" % (board[i][0], board[i][1], board[i][2], board[i][3]));







