#! /usr/bin/env python

total_t = 0;
total_c = 0;
total_g = 0;

line = raw_input();

for l in line:
	if(l == 'T'):
		total_t = total_t + 1;
	elif(l == 'C'):
		total_c = total_c + 1;
	elif(l == 'G'):
		total_g = total_g + 1;

total_min = min(total_t, total_c, total_g);

score = pow(total_t, 2) + pow(total_c, 2) + pow(total_g, 2) + total_min * 7;
print(score);
