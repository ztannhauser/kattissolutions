#! /usr/bin/env python

(r_1, s) = map(int, raw_input().split());
r_2 = 2 * s - r_1;
print(r_2);
