#! /usr/bin/env python

(d, m) = map(int, raw_input().split());

M = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];

s = 3 + d;
for w in range(1, m):
	s = s + M[w - 1];

names = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];

print(names[s % 7]);
