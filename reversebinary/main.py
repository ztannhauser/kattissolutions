#! /usr/bin/env python

x = int(raw_input());

binary = [];
while x:
	binary.insert(0, x % 2);
	x = x / 2;
y = 0;
for i in range(0, len(binary)):
	y = y + pow(2, i) * binary[i];
print(y);
