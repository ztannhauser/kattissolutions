#! /usr/bin/env python

rolls = [];

(n, m) = map(int, raw_input().split());

for i in range(0, n):
	for j in range(0, m):
		s = 2 + i + j;
		while(len(rolls) <= s):
			rolls.append([0, len(rolls)]);
		rolls[s][0] = rolls[s][0] + 1;

rolls.sort();

highest_prob = rolls[len(rolls) - 1][0];
rolls2 = [];

for roll in rolls:
	if(roll[0] == highest_prob):
		rolls2.append(roll[1]);
rolls2.sort();
for r in rolls2:
	print(r);
