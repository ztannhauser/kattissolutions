#! /usr/bin/env python

nondomin_scores = [11, 4, 3, 2, 10, 0, 0, 0];
domin_scores    = [11, 4, 3, 20, 10, 14, 0, 0];

card_numbers = "AKQJT987";

(n_str, domin) = raw_input().split();
n = int(n_str);
domin = domin[0];

total_score = 0;
for i in range(0, n):
	for qqweqwe in range(0, 4):
		line = raw_input();
		score_index = card_numbers.index(line[0]);
		if(line[1] == domin):
			total_score = total_score + domin_scores[score_index];
		else:
			total_score = total_score + nondomin_scores[score_index];
print(total_score);
