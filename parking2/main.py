#! /usr/bin/env python

n = int(raw_input());
for i in range(0, n):
	N = int(raw_input());
	P = map(int, raw_input().split());
	P.sort();
	avg_p = sum(P) / len(P);
	ret = abs(P[0] - avg_p) + abs(P[len(P) - 1] - avg_p);
	for i in range(0, len(P) - 1):
		s = abs(P[i] - avg_p) + abs(P[i + 1] - avg_p);
		ret = ret + min(P[i + 1] - P[i], s);
	print(ret);

