#! /usr/bin/env python

(num_of_junctions, num_of_switches) = map(int, raw_input().split());
if(num_of_switches % 2):
	print("impossible");
else:
	print("possible");
