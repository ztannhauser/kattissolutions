#! /usr/bin/env python

(p1x, p1y) = map(int, raw_input().split());
(p2x, p2y) = map(int, raw_input().split());
(p3x, p3y) = map(int, raw_input().split());

p4x = 0;
p4y = 0;

if(p1x == p2x):
	p4x = p3x;
elif(p2x == p3x):
	p4x = p1x;
else:
	p4x = p2x;
	
if(p1y == p2y):
	p4y = p3y;
elif(p2y == p3y):
	p4y = p1y;
else:
	p4y = p2y;

print("%i %i" % (p4x, p4y));
