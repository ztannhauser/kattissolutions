#! /usr/bin/env python

N = int(raw_input());
for I in range(0, N):
	n = map(int, raw_input().split());
	o = 0;
	s = 2 * n[0];
	for i in range(0, len(n)):
		if(n[i] > s):
			o = o + (n[i] - s);
		s = 2 * n[i];
	print(o);
