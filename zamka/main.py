#! /usr/bin/env python

l = int(raw_input());
d = int(raw_input());
x = int(raw_input());

first = 0;
last = 0;

for e in range(l, d + 1):
	line = "%i" % e;
	s = 0;
	for l in line:
		s = s + (ord(l) - ord('0'));
	if(x == s):
		if(first == 0):
			first = e;
		last = e;

print(first);
print(last);
	
