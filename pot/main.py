#! /usr/bin/env python

N = int(raw_input());
s = 0;

for i in range(0, N):
	n = int(raw_input());
	s = s + pow(n / 10, n % 10);
	
print(s);
