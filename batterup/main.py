#! /usr/bin/env python

N = int(raw_input());
s = map(int, raw_input().split());

numerator = 0.0;
denominator = len(s);

for i in s:
	if(i == -1):
		denominator = denominator - 1;
	else:
		numerator = numerator + i;
		
print(numerator / denominator);
