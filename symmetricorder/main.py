#! /usr/bin/env python

I = 1;
while True:
	N = int(raw_input());
	if(N == 0):
		break;
	else:
		lines = [];
		for i in range(0, N):
			lines.append(raw_input());
		part2 = [];
		for i in range(1, N/2 + 1):
			part2.insert(0, lines.pop(i));
		lines = lines + part2;
		print("SET %i" % I);
		for line in lines:
			print(line);
		I = I + 1;

