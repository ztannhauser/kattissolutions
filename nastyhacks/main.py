#! /usr/bin/env python

n = int(raw_input());
for i in range(0, n):
	(r, e, c) = map(int, raw_input().split());
	d = (e - c) - r;
	if(d > 0):
		print("advertise");
	elif(d < 0):
		print("do not advertise");
	else:
		print("does not matter");
