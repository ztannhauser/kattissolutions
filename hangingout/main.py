#! /usr/bin/env python

(limit, n) = map(int, raw_input().split());
ret = 0;
current = 0;
for i in range(0, n):
	args = raw_input().split();
	x = int(args[1]);
	if(args[0] == "enter"):
		newcurrent = current + x;
		if(newcurrent >= limit):
			ret = ret + 1;
		else:
			current = newcurrent;
	elif(args[0] == "leave"):
		current -= x;
print(ret);
