#! /usr/bin/env python
(x, y, n) = map(int, raw_input().split());

for i in range(1, n + 1):
	if(i % x):
		if(i % y):
			print(i);
		else:
			print("Buzz");
	else:
		if(i % y):
			print("Fizz");
		else:
			print("FizzBuzz");
