#! /usr/bin/env python

while True:
	n = int(raw_input());
	if(n == -1):
		break;
	else:
		total = 0;
		last_time = 0;
		for i in range(0, n):
			(mph, time) = map(int, raw_input().split());
			total = total + mph * (time - last_time);
			last_time = time;
		print("%i miles" % total);

